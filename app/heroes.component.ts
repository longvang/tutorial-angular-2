import {Component,OnInit} from 'angular2/core';
import { Router } from 'angular2/router';

import {Hero} from './hero'; //hero interface
import {HeroDetailComponent} from './hero-detail.component';
import {HeroService} from './hero.service';

@Component({
    selector: 'my-heroes',
    //template
    templateUrl: 'app/heroes.component.html',
    //css
    styleUrls: ['app/heroes.component.css'],
    directives: [HeroDetailComponent]
})
export class HeroesComponent implements OnInit {
    //hero property
    heroes:Hero[];
    // selectedHero used interface Hero
    selectedHero:Hero;
    // constructor providers HeroService
    constructor(private _heroService:HeroService,
                private _router:Router) {}

    getHeroes() {
        this._heroService.getHeroes().then(heroes => this.heroes = heroes);
    }

    ngOnInit() {
        this.getHeroes();
    }

    // function onSelect
    onSelect(hero:Hero) {
        this.selectedHero = hero;
    }
    gotoDetail(){
        this._router.navigate(['HeroDetail', { id: this.selectedHero.id }])
    }
}