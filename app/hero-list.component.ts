import {Component} from 'angular2/core';
import {FetchJsonPipe} from './pipes/fetch-json.pipe';
@Component({
    selector: 'hero-list',
    template: `
    <h4>Heroes from JSON File</h4>
    <div *ngFor="#hero of ('app/heroes.json' | fetch) ">
      {{hero.name}}
    </div>
    <p>Heroes as JSON:
    {{'app/heroes.json' | fetch | json}}
    </p>
  `,
    pipes: [FetchJsonPipe]
})
export class HeroListComponent {
}