import {Component} from 'angular2/core';

@Component({
    selector: 'my-birthday',
    templateUrl:'app/pipes/birthday.component.html'
})
//RouteConfig for heroes
export class BirthdayComponent {
    toggle = true; // start with true == shortDate
    get format() { return this.toggle ? 'shortDate' : 'fullDate'}
    toggleFormat(){ this.toggle = !this.toggle; }
    birthday = new Date (1992,1,27);

}