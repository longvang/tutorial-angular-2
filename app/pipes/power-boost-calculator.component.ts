import {Component} from 'angular2/core';
import {ExponentialStrengthPipe} from './exponential-strength.pipe';
@Component({
    selector: 'power-boost-calculator',
    template: `
    <h1>Power Boost Calculator</h1>
    <div>Normal power: <input [(ngModel)]="power"></div>
    <div>Boost factor: <input [(ngModel)]="factor"></div>
    <p>
      Super Hero Power: {{power | exponentialStrength: factor}}
    </p>
  `,
    pipes: [ExponentialStrengthPipe]
})
export class PowerBoostCalculator {
    power = 5;
    factor = 1;
}