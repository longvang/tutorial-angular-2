import {Pipe, PipeTransform} from 'angular2/core';

@Pipe({name: 'exponentialStrength'})
export class ExponentialStrengthPipe implements PipeTransform {
    transform(value:number, args:string[]) : any {
        return Math.pow(value, parseInt(args[0] || '1', 10));
    }
}