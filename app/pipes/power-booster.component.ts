import {Component} from 'angular2/core';
import {ExponentialStrengthPipe} from './exponential-strength.pipe';

@Component({
    selector: 'power-booster',
    template: `
    <h1>Power Booster</h1>
    <p>
      Super power boost: {{2 | exponentialStrength: 10}}
    </p>
  `,
    pipes: [ExponentialStrengthPipe]
})
export class PowerBooster { }